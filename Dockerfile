
FROM node:18-alpine as base

WORKDIR /text-analyzer

COPY package*.json ./

RUN npm install

COPY --chown=node:node . .

#USER node

EXPOSE 3001

#RUN export NODE_ENV=production

CMD [ "node", "dist/index.js" ]
