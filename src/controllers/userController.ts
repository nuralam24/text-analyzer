import userModel from './../models/user';
import chalk from 'chalk';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from "express";
import dotenv from 'dotenv';
dotenv.config();
import { exit } from 'process';

const projection = {
    password: 0,
    createdAt: 0,
    updatedAt: 0,
    __v: 0
};

// user registration
const registration = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { email, password } = req.body;
        const userExists = await userModel.findOne({ email });
        if (userExists) {
            return res.status(409).json({
                data: null,
                success: false,
                message: `Email already exists!`
            });
        }

        const salt = bcrypt.genSaltSync(10);
        req.body.password = bcrypt.hashSync(password, salt);
        const createNewUser = await userModel.create(req.body);
        if (!createNewUser) {
            return res.status(400).json({
                data: null,
                success: false,
                message: `User registration incomplete!`
            });
        }
        return res.status(201).json({
            success: true,
            message: `User registration successfully!`
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

// user login
const login = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { email, password } = req.body;
        const existingUser = await userModel.findOne({ email });
        if (!existingUser) {
            return res.status(401).json({
                data: null,
                success: false,
                message: `Email didn't match!`
            });
        }

        const passwordMatch = await bcrypt.compare(password, existingUser.password);
        if (!passwordMatch) {
            return res.status(401).json({
                data: null,
                success: false,
                message: `Email or password is incorrect!`
            });
        }
        const TOKEN_SECRET: string | undefined = process.env.TOKEN_SECRET;
        if (!TOKEN_SECRET) {
            console.error(chalk.red.bold('TOKEN_SECRET is not defined in the environment variables!'));
            exit(1);
        }

        const token = jwt.sign({ _id: existingUser._id }, process.env.TOKEN_SECRET!, { expiresIn: '30d' });
        const data = {
            _id: existingUser._id, email: existingUser.email, token
        }
        return res.status(200).json({
            data,
            success: true,
            message: 'Logged in Successfully!',
        });

    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
}

export { registration, login };
