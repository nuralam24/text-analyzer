import passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import mongoose from 'mongoose';
import User from '../models/user';
import dotenv from 'dotenv';
dotenv.config();
import chalk from 'chalk';
import { exit } from 'process';

const TOKEN_SECRET: string | undefined = process.env.TOKEN_SECRET;
if (!TOKEN_SECRET) {
    console.error(chalk.red.bold('TOKEN_SECRET is not defined in the environment variables!'));
    exit(1);
}

const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: TOKEN_SECRET,
};

passport.use(
    new JwtStrategy(opts, async (jwt_payload, done) => {
        try {
            const user = await User.findById(jwt_payload.id);
            if (user) return done(null, user);
            return done(null, false);
        } catch (err) {
            console.error(err);
            return done(err, false);
        }
    })
);

export default passport;
