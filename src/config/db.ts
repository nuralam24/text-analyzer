import { exit } from 'process';
import mongoose from 'mongoose';
import { ConnectOptions } from 'mongoose';
import dotenv from 'dotenv';
dotenv.config();
import chalk from 'chalk';

mongoose.set('strictQuery', false);
const DB_URI: string | undefined = process.env.DB_URI;

if (!DB_URI) {
    console.error('DB_URI is not defined in the environment variables!');
    exit(1);
}

const db = async (app: any): Promise<any> => {
    try {
        const options: ConnectOptions = {
            connectTimeoutMS: 30000,
            tlsInsecure: true
        };

        await mongoose.connect(DB_URI, options);
        console.log(chalk.magenta('Database Connection Succeeded!'));
        return app;
    } catch (err) {
        console.error(err);
        exit(1);
    }
};

export default db;
