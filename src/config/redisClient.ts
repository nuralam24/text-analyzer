import { createClient } from 'redis';

const client = createClient({
    url: 'redis://127.0.0.1:6379',
});

client.on('connect', () => {
    console.log('Redis client connected');
});

client.on('error', (err) => {
    console.error('Redis client error', err);
});

client.connect().catch(console.error);

export default client;

