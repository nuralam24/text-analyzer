import { Router } from 'express';
import verifyToken from '../middlewares/verifyToken';
import permission from '../middlewares/permission';
import {registration, login} from '../controllers/userController';

const router = Router();

router.post('/registration', registration);
router.post('/login', login);

export default router;