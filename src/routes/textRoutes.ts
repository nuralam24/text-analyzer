import express from 'express';
import passport from 'passport';
import { createText, getWordCount, getCharacterCount, getSentenceCount, getParagraphCount, getLongestWords } from '../controllers/textController';
import cache from '../middlewares/cache';

const router = express.Router();

router.post('/', passport.authenticate('jwt', { session: false }), createText);
router.get('/:id/words', passport.authenticate('jwt', { session: false }), cache, getWordCount);
router.get('/:id/characters', passport.authenticate('jwt', { session: false }), cache, getCharacterCount);
router.get('/:id/sentences', passport.authenticate('jwt', { session: false }), cache, getSentenceCount);
router.get('/:id/paragraphs', passport.authenticate('jwt', { session: false }), cache, getParagraphCount);
router.get('/:id/longest-words', passport.authenticate('jwt', { session: false }), cache, getLongestWords);

export default router;
