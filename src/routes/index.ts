import userRouter from './user';
import textRouter from './textRoutes';

const routes = [
    { path: '/user', controller: userRouter },
    { path: '/texts', controller: textRouter }
];

const setupRoutes = (app: any) => {
    for (const route of routes) {
        app.use(`/api/v1${route.path}`, route.controller);
    }
};

export default setupRoutes;
