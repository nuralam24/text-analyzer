import { countWords, countCharacters, countSentences, countParagraphs, longestWordInParagraphs } from '../utils/textUtils';

test('countWords should return the correct word count', () => {
    const text = "The quick brown fox jumps over the lazy dog.";
    expect(countWords(text)).toBe(9);
});

test('countCharacters should return the correct character count', () => {
    const text = "The quick brown fox jumps over the lazy dog.";
    expect(countCharacters(text)).toBe(35);
});

test('countSentences should return the correct sentence count', () => {
    const text = "The quick brown fox jumps over the lazy dog. The lazy dog slept in the sun.";
    expect(countSentences(text)).toBe(2);
});

test('countParagraphs should return the correct paragraph count', () => {
    const text = "The quick brown fox jumps over the lazy dog.\nThe lazy dog slept in the sun.";
    expect(countParagraphs(text)).toBe(2);
});

test('longestWordInParagraphs should return the longest word in each paragraph', () => {
    const text = "The quick brown fox jumps over the lazy dog.\nThe lazy dog slept in the sun.";
    expect(longestWordInParagraphs(text)).toEqual(["jumps", "slept"]);
});
