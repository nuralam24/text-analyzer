import rateLimit from "express-rate-limit";

const serverRequest = rateLimit({
    windowMs: 5 * 60 * 1000, // 5 minutes
    max: 5000, // limit each IP to 5000 requests per windowMs,
    message: "Too many request attempts. Please try again after 5 minutes!",
    statusCode: 429,
    headers: true
});

export { serverRequest };
