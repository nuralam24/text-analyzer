import jwt, { Secret } from 'jsonwebtoken';

const verifyToken = (req: any, res: any, next: any) => {
    if (!req.headers.authorization) {
        return res.status(401).json({
            success: false,
            message: "Unauthorized. Token Required!"
        });
    }
    const token = req.headers['authorization'].replace(/^JWT\s/, '');
    if (!token) {
        return res.status(401).json({
            success: false,
            message: "Token Missing!"
        });
    } else {
        try {
            const tokenSecret = process.env.TOKEN_SECRET || "";
            jwt.verify(token, tokenSecret as Secret, (err: any, decoded: any) => {
                if (err) {
                    return res.status(401).json({
                        success: false,
                        message: "Forbidden. Token Invalid!"
                    });
                }
                req.user = decoded;
                next();
            });
        } catch (err) {
            console.log(err);
            return next(err);
        }
    }
};

export default verifyToken;
