import express, { json, urlencoded } from 'express';
import session, { SessionOptions } from 'express-session';
import cors from 'cors';
import helmet from 'helmet';
import customFormatMorgan from '../utils/loggerMorgan';
import { serverRequest } from './requestAttempt';
import dotenv from 'dotenv';
dotenv.config();

const sessionConfig: SessionOptions = {
    secret: process.env.TOKEN_SECRET || '',
    resave: false,
    saveUninitialized: true,
};

export default (app: express.Application) => {
    app.use(cors());
    app.use(serverRequest);
    app.set('trust proxy', 1);
    app.use(session(sessionConfig));
    app.use('/public', [express.static('public')]);
    app.use(helmet({ crossOriginResourcePolicy: false }));
    app.use(json({ limit: '50mb' }));
    app.use(urlencoded({ limit: '50mb', extended: true }));
    app.use(customFormatMorgan);
};