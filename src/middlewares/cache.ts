import { Request, Response, NextFunction } from 'express';
import redisClient from '../config/redisClient';

const cache = async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;

    try {
        const data = await redisClient.get(id);

        if (data) {
            res.json(JSON.parse(data));
        } else {
            next();
        }
    } catch (err) {
        console.error('Redis cache error', err);
        next();
    }
};

export default cache;
