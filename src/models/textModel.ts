import { Schema, model, Document } from 'mongoose';

interface IText extends Document {
  content: string;
  createdAt: Date;
}

const textSchema = new Schema<IText>({
  content: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
});

const Text = model<IText>('Text', textSchema);

export default Text;
