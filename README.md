# Text Analyzer Tool

## Description
A RESTful API for managing books and authors, built with Node.js, TypeScript, Express, and MongoDB. Includes Single Sign On (SSO).

## Features
- User registration & login. Test View by multiple way
- JWT-based authentication & Single Sign On (SSO)
- Using Redis

## Setup

### Prerequisites
- Docker
- Docker Compose

### Manual Installation

1. Clone the repository
   ```bash
   git clone https://github.com/nuralam24/text-analyzer.git
   cd text-analyzer
   .env file must be added to the root directory. requested to follow .env.example file
   npm install
   npm run dev


### API ENDPOINT
# Auth
- POST: `http://localhost:3001/api/v1/user/registration`
- POST: `http://localhost:3001/api/v1/user/login`

# Text
- POST: `http://localhost:3001/api/v1/texts` - Create a new text
- GET: `http://localhost:3001/api/v1/texts/:id/words` - Get word count of a text
- GET: `http://localhost:3001/api/v1/texts/:id/characters` - Get character count of a text
- GET: `http://localhost:3001/api/v1/texts/:id/sentences` - Get sentence count of a text
- GET: `http://localhost:3001/api/v1/texts/:id/paragraphs` - Get paragraph count of a text
- GET: `http://localhost:3001/api/v1/texts/:id/longest-words` - Get longest words in each paragraph

