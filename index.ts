import http from 'http';
import app from './src/app';
import dotenv from 'dotenv';
import chalk from 'chalk';
import { exit } from 'process';

dotenv.config();
const PORT: string | undefined = process.env.PORT;

if (!PORT) {
    console.error(chalk.red.bold('PORT is not defined in the environment variables!'));
    exit(1);
}

const server = http.createServer(app);
server.listen(PORT, () => {
    console.log(chalk.blue(`Server is running on port: ${PORT}`));
});
